package id.sch.smktelkom_mlg.swipe;

import android.app.Fragment;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.View;

/**
 * Created by SMK TELKOM on 26/05/2018.
 */

public class TabUtama extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    public static TabLayout tabLayout;
    public static ViewPager viewPager;
    public static int int_items = 3;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private OnFragmentInteractionListener mListener;

    {

        public MyAdapter(FragmentManager fm) {
        super(fm);
    }

        @Override
        public Fragment getItem ( int position)
        {
            switch (position) {
                case 0:
                    return new fragment1();
                case 1:
                    return new fragment2();
                case 2:
                    return new fragment3();
            }
            return null;
        }
        @Override
        public int getCount () {
        return int_items;
    }

        @Override
        public CharSequence getPageTitle ( int position){

        switch (position) {
            case 0:
                return "Contoh 1";
            case 1:
                return "Contoh 2";
            case 2:
                return "Contoh 3";
        }
        return null;
    }
    }

    public TabUtama() {
    }

    // TODO: Rename and change types and number of parameters
    public static TabUtama newInstance(String param1, String param2) {
        TabUtama fragment = new TabUtama();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }
    <strong>

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View x = inflater.inflate(R.layout.tab_layout, null);
        tabLayout = x.findViewById(R.id.tabbaru);
        viewPager = x.findViewById(R.id.viewpager);

        viewPager.setAdapter(new MyAdapter(getChildFragmentManager()));

        tabLayout.post(new Runnable() {
            @Override
            public void run() {
                tabLayout.setupWithViewPager(viewPager);
            }
        });
        return x;
    }/strong>

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    class MyAdapter extends FragmentPagerAdapter<
}
