package id.sch.smktelkom_mlg.swipe;

import android.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, fragment1.OnFragmentInteractionListener, fragment2.OnFragmentInteractionListener, fragment3.OnFragmentInteractionListener, fragment4.OnFragmentInteractionListener, TabUtama.OnFragmentInteractionListener {

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        Fragment fragment = null;
        Class fragmentClass = null;

        if (id == R.id.contoh1) { //id pada layout activity_main_drawer.xml
            fragmentClass = fragment1.class;
        } else if (id == R.id.contoh2) {
            fragmentClass = fragment2.class;
        } else if (id == R.id.contoh3) {
            fragmentClass = fragment3.class;
        } else if (id == R.id.contoh4) {
            fragmentClass = fragment4.class;
        } else if (id == R.id.contoh11) {
            <strong > fragmentClass = TabUtama.class;</strong >
        } else if (id == R.id.contoh12) {
            // kode untuk di execute ketika menu camera di klik
        }

        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.framecontoh, fragment).commit();

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
